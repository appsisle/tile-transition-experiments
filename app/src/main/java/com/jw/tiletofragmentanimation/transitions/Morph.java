package com.jw.tiletofragmentanimation.transitions;

import android.transition.Transition;

/**
 * @author Jaroslaw Wisniewski, j.wisniewski@appsisle.com
 */
public interface Morph {
	Transition createFadeAfterMorph(long duration);
	Transition createFadeBeforeMorph(long duration);
	Transition createMorph(long duration, String sharedElementToFade);
}
