package com.jw.tiletofragmentanimation.transitions;

import android.graphics.PointF;
import android.transition.ChangeBounds;
import android.transition.ChangeTransform;
import android.transition.Fade;
import android.transition.Transition;
import android.transition.TransitionSet;
import android.view.animation.AccelerateDecelerateInterpolator;

import com.jw.tiletofragmentanimation.Interpolators;

/**
 * @author Jaroslaw Wisniewski, j.wisniewski@appsisle.com
 */
public class MorphWithoutFade implements Morph {

	// animation percentages
	private static final float begin = 0f;
	private static final float end = 1f;

	/**
	 * Keeps initial visibility (alpha) till the end of the animation.
	 */
	@Override
	public Transition createFadeAfterMorph(long duration) {
		return new Fade()
				.setDuration(duration)
				.setInterpolator(Interpolators.linearWithControlPoints(new PointF(end, begin)));
	}

	/**
	 * Sets the final visibility (alpha) from the beginning of the animation.
	 */
	@Override
	public Transition createFadeBeforeMorph(long duration) {
		return new Fade()
				.setDuration(duration)
				.setInterpolator(Interpolators.linearWithControlPoints(new PointF(begin, end)));
	}

	/**
	 * Changes transform and bounds.
	 */
	@Override
	public Transition createMorph(long duration, String sharedElementToFade) {
		ChangeBounds morphChangeBounds = new ChangeBounds();
		morphChangeBounds.setDuration(duration);
		morphChangeBounds.setInterpolator(new AccelerateDecelerateInterpolator());

		ChangeTransform morphChangeTransform = new ChangeTransform();
		morphChangeTransform.setReparentWithOverlay(true);
		morphChangeTransform.setDuration(duration);
		morphChangeBounds.setInterpolator(new AccelerateDecelerateInterpolator());

		return new TransitionSet()
				.addTransition(morphChangeBounds)
				.addTransition(morphChangeTransform);
	}
}
