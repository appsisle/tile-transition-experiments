package com.jw.tiletofragmentanimation.transitions;

import android.graphics.PointF;
import android.transition.ChangeBounds;
import android.transition.ChangeTransform;
import android.transition.Fade;
import android.transition.Transition;
import android.transition.TransitionSet;

import com.jw.tiletofragmentanimation.FadeTransition;
import com.jw.tiletofragmentanimation.Interpolators;

/**
 * @author Jaroslaw Wisniewski, j.wisniewski@appsisle.com
 */
public class MorphWithSmoothFade implements Morph {

	// animation percentages
	private static final float begin = 0f;
	private static final float end = 1f;

	// time positions
	private static final float fadeDuration = 1f / 4f;
	private static final float morphDelay = 1f / 4f;
	private static final float morphDuration = 1f - morphDelay - fadeDuration;

	/**
	 * Keeps initial visibility (alpha) till end of the morph animation, then switches immediately to the final alpha.
	 */
	@Override
	public Transition createFadeAfterMorph(long duration) {
		return new Fade()
				.setDuration(duration)
				.setInterpolator(Interpolators.linearWithControlPoints(new PointF(morphDelay + morphDuration, begin), new PointF(morphDelay + morphDuration, end)));
	}

	/**
	 * Keeps initial visibility (alpha) till start of the morph animation, then switches immediately to the final alpha.
	 */
	@Override
	public Transition createFadeBeforeMorph(long duration) {
		return new Fade()
				.setDuration(duration)
				.setInterpolator(Interpolators.linearWithControlPoints(new PointF(morphDelay, begin), new PointF(morphDelay, end)));
	}

	/**
	 * Starts with full transparency, then smoothly animates it to full opaque, then reshapes target views by change of {@link ChangeBounds}
	 * and {@link ChangeTransform}. Finally after the bounds change, animates target views back to full transparency.
	 */
	@Override
	public Transition createMorph(long duration, String sharedElementToFade) {
		FadeTransition morphFade = new FadeTransition(0f, 1f);
		morphFade.setDuration(duration);
		morphFade.addTarget(sharedElementToFade);
		morphFade.setInterpolator(Interpolators.linearWithControlPoints(new PointF(morphDelay, end), new PointF(morphDelay + morphDuration, end), new PointF(end, begin)));

		ChangeBounds morphChangeBounds = new ChangeBounds();
		morphChangeBounds.setDuration(duration);
		morphChangeBounds.setInterpolator(Interpolators.linearWithControlPoints(new PointF(morphDelay, begin), new PointF(morphDelay + morphDuration, end)));

		ChangeTransform morphChangeTransform = new ChangeTransform();
		morphChangeTransform.setReparentWithOverlay(true);
		morphChangeTransform.setDuration(duration);
		morphChangeTransform.setInterpolator(Interpolators.linearWithControlPoints(new PointF(morphDelay, begin), new PointF(morphDelay + morphDuration, end)));

		return new TransitionSet()
				.addTransition(morphFade)
				.addTransition(morphChangeBounds)
				.addTransition(morphChangeTransform);
	}
}
