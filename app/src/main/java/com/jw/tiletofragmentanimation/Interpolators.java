package com.jw.tiletofragmentanimation;

import android.graphics.Path;
import android.graphics.PointF;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.PathInterpolator;

/**
 * @author Jaroslaw Wisniewski, j.wisniewski@appsisle.com
 */
public class Interpolators {

	public static Interpolator linearWithControlPoints(PointF... points) {
		Path path = new Path();

		for (PointF point : points) {
			path.lineTo(point.x, point.y);
		}
		path.lineTo(1f, 1f);

		return new PathInterpolator(path);
		//return new LinearInterpolator();
	}
}
