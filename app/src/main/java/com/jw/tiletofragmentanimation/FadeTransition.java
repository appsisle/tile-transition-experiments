package com.jw.tiletofragmentanimation;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.transition.Transition;
import android.transition.TransitionValues;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Fade animation between predefined levels of the alpha channel.
 * @author internet
 */
@TargetApi(21)
public class FadeTransition extends Transition {

	private static final String PROPNAME_BACKGROUND = "android:faderay:background";
	private static final String PROPNAME_TEXT_COLOR = "android:faderay:textColor";
	private static final String PROPNAME_ALPHA = "android:faderay:alpha";

	private float startAlpha;
	private float endAlpha;

	public FadeTransition(final float startAlpha, final float endAlpha) {
		this.startAlpha = startAlpha;
		this.endAlpha = endAlpha;
	}

	public FadeTransition(final Context context, final AttributeSet attrs) {
		super(context, attrs);
	}

	private void captureValues(final TransitionValues transitionValues) {
		transitionValues.values.put(PROPNAME_BACKGROUND, transitionValues.view.getBackground());
		transitionValues.values.put(PROPNAME_ALPHA, transitionValues.view.getAlpha());
		if (transitionValues.view instanceof TextView) {
			transitionValues.values.put(PROPNAME_TEXT_COLOR, ((TextView) transitionValues.view).getCurrentTextColor());
		}
	}

	@Override
	public void captureStartValues(final TransitionValues transitionValues) {
		captureValues(transitionValues);
	}

	@Override
	public void captureEndValues(final TransitionValues transitionValues) {
		captureValues(transitionValues);
	}

	@SuppressLint("NewApi")
	@Override
	public Animator createAnimator(final ViewGroup sceneRoot, final TransitionValues startValues,
								   final TransitionValues endValues) {

		View textView = endValues.view;

		if (startAlpha != endAlpha) {
			textView.setAlpha(endAlpha);
		}

		ObjectAnimator fade = ObjectAnimator.ofFloat(textView, View.ALPHA, startAlpha, endAlpha);
		fade.setInterpolator(getInterpolator());
		return fade;
	}
}