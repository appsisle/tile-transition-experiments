package com.jw.tiletofragmentanimation;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jw.tiletofragmentanimation.transitions.Morph;
import com.jw.tiletofragmentanimation.transitions.MorphWithSmoothFade;
import com.jw.tiletofragmentanimation.transitions.MorphWithoutFade;

/**
 * A placeholder fragment containing a simple view.
 */
public class DashboardFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_dashboard, container, false);

		final View tile = view.findViewById(R.id.tile);
		tile.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onTileClick(tile);
			}
		});

		return view;
	}

	void onTileClick(View tile) {

		// current fragment views
		final View tileFrame = tile.findViewById(R.id.tileFrame);
		final View lightGuide = tile.findViewById(R.id.lightGuide);

		// total animation duration in ms
		final long duration = 500;

		// transition definitions
		Morph morph = new MorphWithSmoothFade();	// change between MorphWithSmoothFade and MorphWithoutFade

		// current fragment configuration
		setExitTransition(morph.createFadeAfterMorph(duration));
		setReenterTransition(morph.createFadeBeforeMorph(duration));

		// new fragment configuration
		Fragment newFragment = new ExpandedTileFragment();

		newFragment.setReturnTransition(morph.createFadeBeforeMorph(duration));
		newFragment.setEnterTransition(morph.createFadeAfterMorph(duration));

		String fadingSharedElement = getString(R.string.tileFrameTransitionName);
		newFragment.setSharedElementEnterTransition(morph.createMorph(duration, fadingSharedElement));
		newFragment.setSharedElementReturnTransition(morph.createMorph(duration, fadingSharedElement));

		// transition configuration
		FragmentTransaction transaction = getFragmentManager().beginTransaction();
		transaction.replace(R.id.fragment_container, newFragment);
		transaction.addToBackStack(null);
		transaction.addSharedElement(tileFrame, getString(R.string.tileFrameTransitionName));
		transaction.addSharedElement(lightGuide, getString(R.string.lightGuideTransitionName));
		transaction.commit();
	}
}
